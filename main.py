
list_A = []
list_B = [5, 1, 7, 7, 25, 45, 23, 23, 0]
list_C = [1, 1, 1, 2, 1, 1, 1]
list_D = [2, 4]
list_E = [4, 1]

def sort_list(list_name):
    n = len(list_name)
    while n > 1:
        for i in range(0, n - 1):
            if list_name[i] > list_name[i + 1]:
                list_name[i], list_name[i + 1] = list_name[i + 1], list_name[i]
            else:
                list_name[i], list_name[i + 1] = list_name[i],  list_name[i + 1]
        n = n-1
    return list_name

def result(list_name):
    print(f"Ciąg liczb: {list_name}")
    sort_list(list_name)
    print(f"Posortowany ciąg liczb:{list_name}")
    print("-----------------")
    
def main():
    result(list_A)
    result(list_B)
    result(list_C)
    result(list_D)

main()


